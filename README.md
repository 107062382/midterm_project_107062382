# Software Studio 2020 Spring
## Midterm Project - Chatroom


### Scoring

| **Basic components**                             | **Score** | **Check** |
| :----------------------------------------------- | :-------: | :-------: |
| Membership Mechanism                              | 15%       | Yes         |
| Host on firebase page                                       | 5%       | Yes         |
| Database read/write                                      | 15%       | Yes         |
| RWD                                   | 15%       | Yes         |
| Topic key functions                                      | 20%       | Yes         |

| **Advanced tools**                               | **Score** | **Check** |
| :----------------------------------------------- | :-------: | :-------: |
| Sign in with Google or other third-party accounts                           | 2.5%       | Yes         |
| Add chrome notification                                  | 5%       | Yes         |
| Use CSS animation                           | 2.5%       | Yes         |
| Deal with messages when sending html code                                         | 5%        | Yes         |

| **Other functions**                         | **Score** | **Check** |
| :----------------------------------------------- | :-------: | :-------: |
| Signin page has background music                                  | 1~10%     | Yes         |
| Signup with email requires entering password and confirm password                                  | 1~10%     | Yes         |
| Could show overflow text for message input                                   | 1~10%     | Yes         |
| Could show friend's email address on top of chatroom and can change into another email address when clicking on another friend's message                                  | 1~10%     | Yes         |
| Could see the time when a friend is added and recorded to see how long since both users become friends in the chatroom                                | 1~10%     | Yes         |
| Each message has sent time displayed below each message bubble                                  | 1~10%     | Yes         |
| Can signin with Facebook account                                  | 1~10%     | Yes         |
| Chrome notification links to another webpage                                  | 1~10%     | Yes         |
| Has alert notifications for errors when signing in or signing up                                  | 1~10%     | Yes         |
Pressing escape when opening a chatroom closes it                                  | 1~10%     | Yes         |

---

| **How to use**                             |
| :----------------------------------------------- |
| 1. Users are required to sign in to the website using the available platforms. Users can choose from signing in through registering email and password, or using their google or facebook account. |
|2. If the user didn't have a google or facebook account, the user must register in the signup page. If signup is successful, then user will be redirected back to signin page and are required to enter their email once again to enter the chatroom.|
|3. Inside the chatroom, there are instructions on how to use the chatroom.|
|4. Users need to find a friend first to start chatting. Users can use the textbox for adding friend and input their friend's email address there. Press enter to add the other user.|
|5. When the user has added a friend to his/her friend list, the friend's profile will be shown in the user's contact list. The time when they become friends in the chatroom is also recorded in the contact list, below the friend's name|
|6. When the user has added a friend, the user can start chatting with his/her friend. The user can type anything he want to send to his friend in the text box in the lower part of the page and also press enter to send the chat. The friend will see the message sent by the user at the same moment the message is sent.|
|7. The message has time of when it is sent, and it is shown below the message bubble.|
|8. The user can switch between chatrooms as he/she like it and can also close the chatroom by pressing escape button in the keyboard if the user wants to close the chatroom.|
|9. If the user is done with his/her business in the chatroom and wants to leave the chatroom, the user can log out of the chatroom by pressing the account dropdown bar in the upper part of the page and press logout.|
|10. Log out redirects the user to the login page and the user can login again anytime he/she wants.|

| **Chatroom design**                             |
| :----------------------------------------------- |
| 1. The chatroom has a unique signin/signup page where the input box for inputting the emails are cascaded in the background, blurring the background behind it rather than covering it |
|2. The theme of the chatroom has a blue background, with the contact display colored blue and has white fonts, while the page where the message is displayed has white background and blue fonts, a combination of simplicity and dynamic design.|

    
### Hosting domain

    "midterm-81b59.firebaseapp.com"

### Others (Optional)

    Have mercy on us :)

<style>
</style>