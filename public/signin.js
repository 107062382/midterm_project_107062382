function initApp() {
    // Login with Email/Password
    var txtEmail = document.getElementById('inputEmail');
    var txtPassword = document.getElementById('inputPassword');
    var btnLogin = document.getElementById('btnLogin');
    var btnGoogle = document.getElementById('btngoogle');
    var btnFacebook = document.getElementById('btnfacebook');
    var btnnew = document.getElementById('btnSignUp');

    btnLogin.addEventListener('click', function () {
        var email = txtEmail.value;
        var password = txtPassword.value;
        firebase.auth().signInWithEmailAndPassword(email, password)
            .then(function () {
                window.location = "main.html";
            })
            .catch(function (error) {
                var errorMessage = error.message;
                create_alert(errorMessage);
                txtEmail.value = "";
                txtPassword.value = "";
            });
    });

    btnGoogle.addEventListener('click', function () {
        var provider = new firebase.auth.GoogleAuthProvider();
        firebase.auth().signInWithPopup(provider)
            .then(function () {
                window.location = "main.html";
            })
            .catch(function (error) {
                var errorMessage = error.message;
                create_alert(errorMessage);
                txtEmail.value = "";
                txtPassword.value = "";
            });
    });

    btnFacebook.addEventListener('click', function () {
        var provider = new firebase.auth.FacebookAuthProvider();
        firebase.auth().signInWithPopup(provider)
            .then(function () {
                window.location = "main.html";
            })
            .catch(function (error) {
                var errorMessage = error.message;
                create_alert(errorMessage);
                txtEmail.value = "";
                txtPassword.value = "";
            });
    });

    btnnew.addEventListener('click', function () {
        window.location = "signup.html";
    });

}

function create_alert(message) {
    var alertarea = document.getElementById('custom-alert');
    str_html = "<div class='alert alert-danger alert-dismissible fade show' role='alert'><strong>Error! </strong>" + message + "<button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button></div>";
    alertarea.innerHTML = str_html;
}

window.onload = function () {
    initApp();
}