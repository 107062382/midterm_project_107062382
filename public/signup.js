function initApp() {
    // Login with Email/Password
    var txtEmail = document.getElementById('inputEmail');
    var txtPassword = document.getElementById('inputPassword');
    var checkpass = document.getElementById('checkPassword');
    var returnsign = document.getElementById('returnsign');
    var btnsign = document.getElementById('btnsign');
    returnsign.addEventListener('click', function () {
        window.location = "signin.html";
    });

    btnsign.addEventListener('click', function () {
        var email = txtEmail.value;
        var password = txtPassword.value;
        var confirm = checkpass.value;
        
        if (confirm == password) {
            create_alert("success", "You could sign in right now!");
            firebase.auth().createUserWithEmailAndPassword(email, password)
            .then(function () {
                txtEmail.value = "";
                txtPassword.value = "";
                window.location = "signin.html"
            })
            .catch(function (error) {
                var errorMessage = error.message;
                create_alert("error", errorMessage);
                txtEmail.value = "";
                txtPassword.value = "";
            });
        }
        else {
            var errorMessage = "Password and confirmation password is wrong";
            create_alert("error", errorMessage);
        }
    });

}

function create_alert(type, message) {
    var alertarea = document.getElementById('custom-alert');
    if (type == "success") {
        str_html = "<div class='alert alert-success alert-dismissible fade show' role='alert'><strong>Success! </strong>" + message + "<button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button></div>";
        alertarea.innerHTML = str_html;
    }
    else if (type == "error") {
        str_html = "<div class='alert alert-danger alert-dismissible fade show' role='alert'><strong>Error! </strong>" + message + "<button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button></div>";
        alertarea.innerHTML = str_html;
    }
}

window.onload = function () {
    initApp();
}