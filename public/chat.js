var user_email = '';
function notifyMe() {
    if (!("Notification" in window)) {
      alert("This browser does not support system notifications");
    }
    else if (Notification.permission === "granted") {
      notify();
    }
    else if (Notification.permission !== 'denied') {
      Notification.requestPermission(function (permission) {
        if (permission === "granted") {
          notify();
        }
      });
    }
    
    function notify() {
      var notification = new Notification("HEY THERE! Welcome to JJ's chatroom!", {
        body: "Check out my YouTube channel as well!",
      });
  
      notification.onclick = function () {
        window.open("https://www.youtube.com/channel/UCqjpEduPLC0BWS3u9jrpOAg");      
      };
      setTimeout(notification.close.bind(notification), 7000); 
    }
}

function init() {
    notifyMe();
    firebase.auth().onAuthStateChanged(function(user) {
        var menu = document.getElementById('dynamic-menu');
        if (user) {
            user_email = user.email;
            menu.innerHTML = "<span class='dropdown-item'>" + user.email + "</span><span class='dropdown-item' id='logout-btn'>Logout</span>";
            var logout_button = document.getElementById('logout-btn');
            logout_button.addEventListener('click', function() {
                firebase.auth().signOut()
                    .then(function() {
                        alert('Sign Out!')
                        window.location = "signin.html";
                    })
                    .catch(function(error) {
                        alert('Sign Out Error!')
                    });
            });
        } else {
            menu.innerHTML = "<a class='dropdown-item' href='signin.html'>Login</a>";
            document.getElementById('post_list').innerHTML = "";
        }
    });
    document.onkeydown = function(e) {
        var key = e.keyCode;
        if(key == 27) {
            document.getElementById('msglist').innerHTML = '';
        }
    }

    post_txt = document.getElementById('mesg');
    addfriend = document.getElementById('add')

    document.getElementById('search').onkeydown = function(e) {
        var key = e.keyCode;
        var date_string = new Date().toLocaleString();
        if(addfriend.value != "" && key == 13) {
            var addz = addfriend.value;
            addz = addz.replace(/</g,'&lt').replace(/>/g,'&gt');
            var newpostref = firebase.database().ref('chatroom').push();
            newpostref.set({
                email1: user_email,
                email2: addz,
                date_add: date_string
            });
            addfriend.value = "";
        }
    }

    var friendname = "<div class = 'conversation' onclick = 'switchroom(\""
    var cls1 = "><img src = 'jj.jpg' alt = 'Jonathan Jufeno'><div class = 'friendname'>"
    var cls2 = "</div><div class = 'date'>"
    var cls3 = "</div><div class = 'msg'>Since "
    var cls4 = "</div></div>"

    var friendRef = firebase.database().ref('chatroom');
    var total = [];
    var first = 0;
    var second = 0;

    friendRef.on('child_added', function(contact) {
        second += 1;
        if (second > first) {
            var friendData = contact.val();
            if (contact.val().email1 == user_email) {
                total[total.length] = friendname + contact.key + "\")'" + cls1 + friendData.email2 + cls2 + cls3 + friendData.date_add + cls4;
            }
            else if (contact.val().email2 == user_email) {
                total[total.length] = friendname + contact.key + "\")'" + cls1 + friendData.email1 + cls2 + cls3 + friendData.date_add + cls4;
            }
            document.getElementById('convo').innerHTML = total.join('');
            first = total.length;
        }
    })

    document.getElementById('mesg').onkeydown = function(e) {
        var key = e.keyCode;
        var date_time = new Date().toLocaleTimeString();
        if(post_txt.value != "" && key == 13) {
            var addz = post_txt.value;
            addz = addz.replace(/</g,'&lt').replace(/>/g,'&gt');
            var newpostref = firebase.database().ref('chatroom/' + roomid + '/chats').push();
            newpostref.set({
                email: user_email,
                data: addz,
                time: date_time
            });
            post_txt.value = "";
        }
    }
}

var roomid = '';
function switchroom(id) {
    roomid = id;
    document.getElementById('msglist').innerHTML = '';

    var friendeml = firebase.database().ref('chatroom/' + roomid);
    var title = "<span>"
    var clst = "<div class='flame-wrapper'><div class='flame red'></div><div class='flame orange'></div><div class='flame gold'></div><div class='flame white'></div></div></span>"
    var titlename = [];

    friendeml.on("value", function(snapshot) {
        if (snapshot.val().email1 == user_email) {
            titlename[titlename.length] = title + snapshot.val().email2 + clst;
        }
        else if (snapshot.val().email2 == user_email) {
            titlename[titlename.length] = title + snapshot.val().email1 + clst;
        }
    })
    document.getElementById('chat-title').innerHTML = titlename.join('');

    var postsRef = firebase.database().ref('chatroom/' + roomid + '/chats');
    var total_post = [];
    var first_count = 0;
    var second_count = 0;

    var currentuser = user_email;
    var usermsg = "<div class = 'msgrow me'><div class = 'msgcontent'><div class = 'msgtext'>"
    var cls1 = "</div>"
    var time = "<div class = 'msgtime'>"
    var backmsg = "</div></div></div>"
    
    var frenmsg = "<div class = 'msgrow her'><div class = 'msgcontent'><div class = 'msgtext'>"
    var fbackmsg = "</div></div></div>"

    postsRef.on('child_added', function(data) {
        second_count += 1;
        if (second_count > first_count) {
            var childData = data.val();
            if (currentuser == data.val().email) {
                total_post[total_post.length] = usermsg + childData.data + cls1 + time + childData.time + backmsg;
                titlename[titlename.length] = titlename;
            }
            else {
                total_post[total_post.length] = frenmsg + childData.data + cls1 + time + childData.time + fbackmsg;
            }
            document.getElementById('msglist').innerHTML = total_post.join('');
            first_count = total_post.length;
        }
    });
}

window.onload = function() {
    init();
}